let collection = [];

// Write the queue functions below.


function print() {
    //Print queue elements.
    return collection;
}

function enqueue(element){
    collection.push(element);
    return collection;
    //console.log(collection[0]);
}

function dequeue(){
    collection.shift();
    return collection;
}

function front(){
    return collection[0];
}

function size(){
    return collection.length;
}

function isEmpty(){
    if (collection.length == 0){
        return true;
    } else {
        return false;
    }
}

function node(value){
    this.value = null,
    this.next = null
}

function queue (value){
    this.firstNode = null;
    //this.lastNode = null;
    this.size = 0;
    this.enqueue = (value) => {
        let newNode = new node(value);

        if (this.firstNode == null){
            this.firstNode = newNode;
            //this.lastNode = newNode;
        } else {
            // let tempNode = this.first;
            // this.firstNode = newNode;
            this.firstNode.next = newNode;
        }
        this.size++;
    }
    this.dequeue = (value) => {
        if (this.firstNode !== null){
            let tempNode = this.firstNode.next;
            this.firstNode = tempNode;
        }
        this.size--;
    }
}


module.exports = {
    print
    ,enqueue
    ,dequeue
    ,front
    ,size
    ,isEmpty
};